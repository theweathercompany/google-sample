//
//  GOOGSomeFile.m
//  GoogleSample
//
//  Created by Fussell, Randy on 6/17/15.
//  Copyright (c) 2015 The Weather Channel. All rights reserved.
//

#import "GOOGSomeFile.h"

//Step 3: Comment out import for beta 12
//#import <GoogleAds-IMA-iOS-SDK-For-AdMob/IMAAdError.h>

//Step 4: Uncomment import for beta 14
//@import GoogleInteractiveMediaAds;
#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>

//Step 5: Update podspec

//Step 6: Commit/Push changes and tag commit 1.3

//Step 7: Lint pod with:
//pod spec lint --use-libraries --verbose --allow-warnings
//Attempt to publish pod with:
//pod repo push Google-Sample-Podspec --use-libraries --allow-warnings


@implementation GOOGSomeFile

-(void)someMethod{
    IMAAdError *error =  nil; //Using IMA class.
    error = nil; // Surpress warning
    NSLog(@"something");
}

@end
