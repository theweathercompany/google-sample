//
//  ViewController.m
//  GoogleSample
//
//  Created by Fussell, Randy on 6/17/15.
//  Copyright (c) 2015 The Weather Channel. All rights reserved.
//

#import "ViewController.h"

//Testing importing of pod
#import "GOOGSomeFile.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Pod spec repo
    // git@bitbucket.org:theweathercompany/google-sample-podspecs.git
    
    
    GOOGSomeFile *someClass; //Do some thing.
    someClass = nil; // Stopping unused warning...
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
