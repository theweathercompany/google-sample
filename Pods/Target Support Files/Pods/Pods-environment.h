
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Google-Mobile-Ads-SDK
#define COCOAPODS_POD_AVAILABLE_Google_Mobile_Ads_SDK
#define COCOAPODS_VERSION_MAJOR_Google_Mobile_Ads_SDK 7
#define COCOAPODS_VERSION_MINOR_Google_Mobile_Ads_SDK 3
#define COCOAPODS_VERSION_PATCH_Google_Mobile_Ads_SDK 1

// GoogleAds-IMA-iOS-SDK-For-AdMob
#define COCOAPODS_POD_AVAILABLE_GoogleAds_IMA_iOS_SDK_For_AdMob
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 3.0.beta.14.

